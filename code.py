import time
import microcontroller
import neopixel
import board
import busio
import adafruit_drv2605
import time
import math
import pwmio
import digitalio
import adafruit_lis2mdl

def hsv_to_rgb(h, s, v):
    if (s < 0): s = 0
    if (v < 0): v = 0
    if (s > 1): s = 1
    if (v > 1): v = 1
    S = s
    V = v
    H = (h % 360) / 60
    i = int(H)
    f = H - i
    p1 = V * (1 - S)
    p2 = V * (1 - (S * f))
    p3 = V * (1 - (S * (1 - f)))
    if  (i == 0):
        R = V
        G = p3
        B = p1
    elif (i == 1):
        R = p2
        G = V
        B = p1
    elif (i == 2):
        R = p1
        G = V
        B = p3
    elif (i == 3):
        R = p1
        G = p2
        B = V
    elif (i == 4):
        R = p3
        G = p1
        B = V
    else:
        R = V
        G = p1
        B = p2
    return((R * 255, G * 255, B * 255))


def getFracColour(theta):
    # top: red, right: yellow, bottom: [dark] green, left: blue
    vs = 10 # starting brightness (1-100)
    theta = ((theta % 360) + 360) % 360
    v = vs
    s = 1
    if ((theta >= 0) and (theta <  90)):
      h = (theta * 60) / 90
    elif ((theta >=  90) and (theta < 180)):
      h = ((theta - 90) * 60) / 90 + 60
      v = (100 - ((theta - 90) / 180)) * (vs / 100)
    elif ((theta >= 180) and (theta < 270)):
      h = ((theta - 180) * 120) / 90 + 120
      v = (((theta - 180) / 180) + 50)  * (vs / 100)
    else: # if ((theta >= 2700) and (theta < 3600))
      h = ((theta - 270) * 120) / 90 + 240
    retVal = hsv_to_rgb(h, s, v / 100)
    return retVal

dc = 65535 // 16

x_min = -50
x_max = 10

y_min = -40
y_max = 20

z_min = -70
z_max = 40

print("\nloading Digital stuff")
pixels = neopixel.NeoPixel(board.NEOPIXEL, 1)
pixels.fill((64,64,64))
time.sleep(0.5)

i2c = busio.I2C(board.SCL1, board.SDA1) # QT Py RP2040 STEMMA connector
sensor = adafruit_lis2mdl.LIS2MDL(i2c)
pixels.fill((1,1,1))
button = digitalio.DigitalInOut(board.BUTTON)

btnCount = 0
lastbtnState = button.value

drv1 = adafruit_drv2605.DRV2605(i2c) # haptic controller breakout

goodSeqs = [65, # transition hum 2 80%
            15, # 750 ms alert 100%
            76, # Transition ramp down long sharp 1 - 100% to 0
            112, # Transition ramp up long sharp 0 to 50%
            95, # Transition ramp down long smooth 2 - 50% to 0
            107, # Transition ramp up long smooth 2 - 0 to 50%
            88] # Transition ramp up long sharp 1 - 0 to 100%
seqIndex = 0

drv1.sequence[0] = adafruit_drv2605.Effect(goodSeqs[seqIndex])
drv1.play()
lastPlay = 0

while True:
    mag_x, mag_y, mag_z = sensor.magnetic
    if(mag_x < x_min):
        x_min = mag_x
    if(mag_x > x_max):
        x_max = mag_x
    if(mag_y < y_min):
        y_min = mag_y
    if(mag_y > y_max):
        y_max = mag_y
    if(mag_z < z_min):
        z_min = mag_z
    if(mag_z > z_max):
        z_max = mag_z
    norm_x = 2 * (mag_x-x_min) / (x_max-x_min) - 1
    norm_y = 2 * (mag_y-y_min) / (y_max-y_min) - 1
    norm_z = 2 * (mag_z-z_min) / (z_max-z_min) - 1
    mag_thetaXY = (math.atan2(norm_y, norm_x) + math.pi) / (math.pi)
    mag_thetaYZ = (math.atan2(norm_z, norm_y) + math.pi) / (math.pi)
    mag_thetaXZ = (math.atan2(norm_z, norm_y) + math.pi) / (math.pi)
    mag_selectedAxis = mag_thetaXY
    if((btnCount % 3) == 1):
        mag_selectedAxis = mag_thetaYZ
    elif((btnCount % 3) == 2):
        mag_selectedAxis = mag_thetaXZ

    if((mag_selectedAxis >= 0.9) and (mag_selectedAxis <= 1.1) and (lastPlay > 10)):
        drv1.sequence[0] = adafruit_drv2605.Effect(goodSeqs[seqIndex])
        drv1.play()
        seqIndex = (seqIndex + 1) % len(goodSeqs)
        lastPlay = 0
    if(lastPlay % 10 == 1):
        print("%d: %f" % (btnCount % 3, mag_selectedAxis))


    pVal = getFracColour(180 * mag_selectedAxis)
    pixels.fill(pVal)
    if((button.value != lastbtnState) and (button.value)):
        btnCount = (btnCount + 1) % 3
        if(btnCount == 0):
            drv1.sequence[0] = adafruit_drv2605.Effect(1)
            drv1.sequence[1] = adafruit_drv2605.Effect(0)
        elif(btnCount == 1):
            drv1.sequence[0] = adafruit_drv2605.Effect(1)
            drv1.sequence[1] = adafruit_drv2605.Effect(1)
            drv1.sequence[2] = adafruit_drv2605.Effect(0)
        else:
            drv1.sequence[0] = adafruit_drv2605.Effect(1)
            drv1.sequence[1] = adafruit_drv2605.Effect(1)
            drv1.sequence[2] = adafruit_drv2605.Effect(1)
            drv1.sequence[3] = adafruit_drv2605.Effect(0)
        drv1.play()
        print('Button pressed: %d' % btnCount)
    if(button.value != lastbtnState):
        lastbtnState = button.value
    lastPlay += 1
    time.sleep(0.1)