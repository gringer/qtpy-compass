# Piezoelectric Compass

Code to convert compass bearing information into piezoelectric
vibrations, for use in a compass necklace

Ingredients:

* QT Py RP2040 [microcontroller] [#4900]
* Adafruit Triple-axis Magnetometer - LIS2MDL - STEMMA QT / Qwiic [#4488]
* 3 * Piezoelectric speakers [e.g. #1740]
* STEMMA QT Cable [e.g. #4210]

Method:

1. Connect RP2040 to magnetometer using cable
1. Connect speaker 1 between GND and A0
1. Connect speaker 2 between GND and A2
1. Connect speaker 3 between GND and TX
1. Upload these repository files onto CIRCUITPY drive
1. Press button (BOOT) to turn sound on and off
